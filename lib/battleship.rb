require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_reader :board, :player

  def initialize (player = HumanPlayer.new("Cailin"), board=Board.new)
    puts "Welcome to the semi-functional Battleship Game!"
    @player = player
    @board = board
    @board.populate_grid
    @myboard = Board.new
  end

  def attack(pos)
    if @board.grid[pos[0]][pos[1]].nil?
      puts "miss..."
      @myboard.grid[pos[0]][pos[1]] = :m
    end
    puts "woops, you already shot there!" if @board.grid[pos[0]][pos[1]] == :x
    if @board.grid[pos[0]][pos[1]] == :s
      puts "Congrats, you hit a ship"
      @myboard.grid[pos[0]][pos[1]] = :x
    end
    @board.grid[pos[0]][pos[1]] = :x

  end

  def count
    board.count
  end

  def board
    @board
  end

  def myboard
    @myboard
  end

  def play
    until game_over?
      @myboard.display
      puts "There are #{count} ships left to destroy"
      play_turn
    end
  end

  def play_turn
    target = @player.get_play
    if @board.in_range?(target)
      attack(target)
    else
      puts "That is not a valid move! try again"
      play_turn
    end
  end

  def game_over?
    if board.won?
      puts "Congraguritos, you win"
      exit
      return true
    else
      return false
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  game = BattleshipGame.new
  game.play
end
