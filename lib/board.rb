class Board

    attr_accessor :grid

  def initialize (grid=Board.default_grid)
    @grid = grid
    @count = 0
  end

  def self.default_grid
    Array.new(10) { Array.new(10) {nil} }
  end

  def grid
    @grid
  end

  def display
    @grid.each do |row|
      row.each do |cell|
        print " #{cell}" unless cell.nil?
        print " o" if cell.nil?
      end
      puts "\n"
    end

  end

  def [](pos)
    self.grid[pos[0]][pos[1]]
  end

  def []=(pos, value)
    self.grid[pos[0]][pos[1]] = value
  end

  def count
    @count = self.grid.flatten.count(:s)
  end

  def empty?(pos="llama")
    if pos == "llama"
      return true if count == 0
      return false
    else
      return grid[pos[0]][pos[1]] != :s
    end
  end

  def place_random_ship
    raise "Board is full!" if full?

    @grid[rand(grid.length)][rand(grid.length)] = :s
  end

  def full?
    self.grid.flatten.all? {|el| el == :s}
  end

  def in_range?(pos)
    if pos[0] && pos[1]
      if pos[0] <= @grid.length
        return true if pos[1] <= @grid[0].length
      end
    end
    return false
  end

  def populate_grid
   ships = 20
   ships.times do
     begin
       place_random_ship
     rescue
       retry
     end
   end
  end

  def won?
    return true if count == 0
  end

end
